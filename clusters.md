## Understanding Kubernetes (a little)

At its most basic level, Kubernetes is a framework for managing virtual clusters, similar to virtual machines (VMs), and in fact requires virtualization support. A Kubernetes **cluster** is an abstraction which refers to a collection of **nodes** - a master node for orchestration, worker nodes for deployment. 
These **nodes** are virtual, but can be set up as separate physical machines. **Nodes** in a cluster share resources and manage them dynamically as needed. 

A **pod** is another abstraction defined as a group of **containers**, each of which can be configured to run a particular application, for instance. 

A **deployment** is essentially a set of instructions for the cluster to automatically spawn pods based on predefined desired container states. 

If this seems confusing, that's because it is. Daniel Sanche offers some excellent introductory material to Kubernetes that helped me make a little more sense of how Kubernetes works:  
	- [Kubernetes 101](https://medium.com/google-cloud/kubernetes-101-pods-nodes-containers-and-clusters-c1509e409e16)  
	- [Kubernetes Deployment](https://medium.com/google-cloud/kubernetes-110-your-first-deployment-bf123c1d3f8)

## Setting Up Your Cluster

Now that we've got a cluster up and running, we need to set up our pods with containers and get things going!  
This can be done in two main ways: 

- Directly pass your specifications to `kubectl` in command line
    - You can do a `$ kubectl --help` to see what options are available
    - These are, however, ephemeral and leave no log/trace in case we want to know what was recently done to a cluster.

- "Manifest" files, YAMLs which describes a variety of [kubernetes API objects](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15). 
    - **It is highly recommended you get somewhat familiar with these objects and how to define them.**

A manifest file, being a YAML, is very similar to a JSON, and essentially lists out key-value maps which predefine your pod, deployment, etc.  
Each YAML consists of:  

- apiVersion 
- kind (ex. Pod, Deployment, ReplicationController, Service, Namespace, Node)
- metadata
- spec (specific to the type of object, but basically here's where you configure your containers)  

As far as I can tell, it doesn't matter where your manifests live, as long as you pass the appropriate paths in your `kubectl` commands. 

Here are a couple of example `manifest.yaml` files I found around the web: 

```apiVersion: v1
kind: Pod
metadata:
  name: test-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox
    command: ['sh', '-c', 'echo Hello Kubernetes! && sleep 3600']
```

```apiVersion: apps/v1
kind: Deployment
metadata:
  name: cdms-deployment
spec:
  selector:
    matchLabels:
      app: nginx            
  replicas: 1 
  template:
    metadata:
      labels:
        app: nginx          
    spec:
      containers:
      - name: nginx
        image: detlab/centroot:
        ports:
        - containerPort: 80
```

## TODO

- create a cdms deployment 
    - for now can just be local
    - maybe use [centroot image](https://gitlab.com/glass-ships/centroot) at first?
- expose it to external network
    - access remotely
- mount volumes in pods

- Enable kubernetes dashboard
    - `kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta1/aio/deploy/recommended.yaml`