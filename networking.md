## Networking with your Cluster

One thing we definitely want to be able to do is expose a pod to the external network. 

This can be done with a **LoadBalancer** service.  
        
From [minikube documentation](https://minikube.sigs.k8s.io/docs/tasks/loadbalancer/): 

A LoadBalancer service is the standard way to expose a service to the internet. 
With this method, each service gets it’s own IP address.

The `minikube tunnel` command can be used to expose LoadBalancer Services. It will run until Ctrl-C is hit.  
Example output: 

```
$ minikube tunnel

out/minikube tunnel
Password: *****
Status:
        machine: minikube
        pid: 59088
        route: 10.96.0.0/12 -> 192.168.99.101
        minikube: Running
        services: []
    errors:
                minikube: no errors
                router: no errors
                loadbalancer emulator: no errors
```

`minikube tunnel` runs as a separate daemon, creating a network route on the host to the service CIDR of the cluster using the cluster’s IP address as a gateway. The tunnel command exposes the external IP directly to any program running on the host operating system.  