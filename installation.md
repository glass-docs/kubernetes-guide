## Installation

1. Confirm virtualization is supported on your system:  
	a. `$ grep -E --color 'vmx|svm' /proc/cpuinfo`  

2. Add Kubernetes yum repository:  
	a. Edit `/etc/yum.repos.d/kubernetes.repo` and add: 
	```
	[kubernetes]  
	name=Kubernetes  
	baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64  
	enabled=1  
	gpgcheck=1  
	repo_gpgcheck=1  
	gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg  
	```
3. Install kubectl  
	a. `$ sudo yum install -y kubectl`

4. Install a Hypervisor (KVM2 recommended):  
	a. install dependencies  
		- `$ sudo yum install libvirt libvirt-daemon-kvm qemu-kvm`  
	b. download latest version of kvm2  
		- `$ curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2`  
	c. install  
		- `$ sudo install docker-machine-driver-kvm2 /usr/local/bin/`  
	d. start and verify libvirtd service  
		- ``$ sudo systemctl enable libvirtd.service``  
		- ``$ sudo systemctl start libvirtd.service``  
		- ``$ sudo systemctl status libvirtd.service``

	e. add user to libvirt group and join in current session:  
		- `$ sudo usermod -a -G libvirt $(whoami)`  
		- `$ newgrp libvirt`  

5. Download and install `minikube`  
    - Install method:  
        - `$ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64`
        - `$ sudo install minikube-linux-amd64 /usr/local/bin/minikube && rm minikube`  

6. Configure minikube to use kvm2:  
	- `$ minikube config set vm-driver kvm2`

7. Start up a cluster!  
	- `$ minikube start -p <name your cluster>`  
	- Note that `sudo` is not required to run minikube commands; it is in fact discouraged.  

- **Minikube Notes**: 
	- [Official minikube documentation](https://minikube.sigs.k8s.io/docs/reference/)  for more cluster creation options
	- [Minikube Cheatsheet by dennyzhang](https://github.com/dennyzhang/cheatsheet.dennyzhang.com/blob/master/cheatsheet-minikube-A4/README.org) for quick reference  

- **KVM2 Note**: 
	-If at any point you'd like to update KVM2, you can simply rerun the command in steps 4b and c:  
    `curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2 && sudo install docker-machine-driver-kvm2 /usr/local/bin/ && rm docker-machine-driver-kvm2`  

#### Now that you've got everything installed and ready to go, let's look at [configuring a cluster](clusters.md).