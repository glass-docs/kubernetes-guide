# Kubernetes Cluster Setup Guide

**[NOTE]** This guide written for versions:
- kubectl client v1.14
- kubectl server v1.15
- minikube v1.3.1
- docker-machine-driver-kvm2 v1.3.1

## Contents

I. Overview  
II. Installation  
III. Understanding Kubernetes (a little)  
IV. Setting Up Your Cluster  
TODO. 

## Overview

This document is intended to guide you through the installation, creation, and configuration of a Kubernetes (k8s) cluster with minimal to no  prior k8s knowledge. It can be confusing, in large part due to the documentation (there's a lot of it, and it's pretty fragmented). My hope is that I can collect, consolidate, and distill as much of the most helpful information as possible. I'll also try to provide useful links and relevant documentation  along the way, so if anything seems missing or kinda vague, let me know and we'll work on fixing it!



As far as software goes, there are some options and constants. We'll be using:  
- [minikube](https://minikube.sigs.k8s.io/docs/overview/)
- [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)
- KVM2

The cluster creation part is kind of up to the user; you can use [Google Compute Engine](https://cloud.google.com/compute/), [k3s](https://k3s.io)... 
However, once you've got a cluster, `kubectl` is used for configuration. 

For this guide, I'll show you how to get set up with `minikube`, which is lightweight, relatively easy to use, and perfect for local, offline clusters. This will do just fine for our testing purposes.

Optional bonus software - [Helm, a Kubernetes packages manager](https://helm.sh/docs/)

The basic gist of our setup is this:
- `minikube` creates a cluster. There's some stuff you can tweak with flags and options, but mostly...
- `kubectl` is what's used to configure pods, deployment, etc. 


I'll try to provide links to helpful/relevant documentation as I go along, and if there's anything that seems missing or important, let me know! I'll try to get it added in. 

Alright! Before we dig in too much, let's get everything installed and working - then we can start playing around.

#### Head on over to the [installation page](./installation.md) to get started.